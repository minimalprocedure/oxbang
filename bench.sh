#!/bin/sh

IP=127.0.0.1
PORT=3000
THREADS=4
CONNS=100
DURATION=60s
PATH=/home/nissl/.local/bin/Network/wrk2:$PATH
export PATH

exec wrk \
    -t$THREADS -c$CONNS -d$DURATION \
    --timeout 2000 \
    -R 30000 --latency \
    -H 'Connection: keep-alive' \
    "http://$IP:$PORT"
