(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)


module Reporter = Reporter [@@warning "-27"]

let src = Logs.Src.create "oxbang" ~doc:"oxbang logger"

let () = Logs.set_reporter (Logs_fmt.reporter ())
and () = Logs_threaded.enable ()
and () = Logs.Src.set_level src (Some Debug)
and () = Fmt_tty.setup_std_outputs ()

let pp f msg =
  let now = Ptime_clock.now () |> Ptime.to_rfc3339 in
  Format.fprintf f "[%s] %s" now msg
;;

let pp2 f msg =
  let now = Ptime_clock.now () |> Ptime.to_rfc3339 in
  Format.kasprintf f "[%s] %a" now msg
;;

let exn ex = Logs.warn (fun f -> f "%a" Eio.Exn.pp ex)
let info msg = Logs.info ~src (fun f -> f "%a" pp msg)
let debug msg = Logs.debug ~src (fun f -> f "%a" pp msg)
let error msg = Logs.err ~src (fun f -> f "%a" pp msg)
let warn msg = Logs.warn ~src (fun f -> f "%a" pp msg)


