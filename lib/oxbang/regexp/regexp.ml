(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

type regex_engine =
  | PerlEngine
  | PcreEngine

let compile_regex ?(engine = PcreEngine) regex_str =
  match engine with
  | PerlEngine -> Re.Perl.re ~opts:[ `Dotall; `Caseless ] regex_str |> Re.Perl.compile
  | PcreEngine -> Re.Pcre.regexp ~flags:[ `CASELESS ] regex_str
;;

let split_on_comma = Re.split (compile_regex ",|;")
let group_get = Re.Group.get
let contains s source = Re.execp (compile_regex s) source
