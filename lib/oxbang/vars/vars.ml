(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

open Types

let str_of_var = function
  | Vars.VStrL v -> String.concat ";" v
  | Vars.VIntL v -> List.map (fun i -> string_of_int i) v |> String.concat ";"
  | Vars.VStr v -> v
  | Vars.VInt v -> string_of_int v
;;

let par_to_svar k v =
  match v with
  | v :: [] -> k, Vars.VStr v
  | _ -> k, Vars.VStrL v
;;

let par_to_ivar k v =
  match v with
  | v :: [] -> k, Vars.VInt (int_of_string v)
  | _ -> k, Vars.VIntL (List.map (fun i -> int_of_string i) v)
;;

let process_var kv =
  let rex = Regexp.compile_regex {|:*(([s,i]?)\[?)(\w+)|} in
  let k, v = kv in
  match Re.exec_opt rex k with
  | Some found ->
    let m = Regexp.group_get found in
    let t = m 2
    and k = m 3 in
    (match t with
     | "s" -> par_to_svar k v
     | "i" -> par_to_ivar k v
     | _ -> par_to_svar k v)
  | None -> par_to_svar k v
;;

let is_var part = String.starts_with ~prefix:":" part
