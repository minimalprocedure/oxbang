(** * Copyright (c) 2022 - 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

(* Workaround for ppx_yojson_conv: Add explicit opens for ppx_yojson_conv v0.16.0 *)
open Ppx_yojson_conv_lib.Yojson_conv.Primitives
(* -- *)

module Json = Yojson.Safe

let appname = "OxBang"
let version_major = 1
let version_minor = 0
let version_patch = 0
let version_pre_release = "0.dev"

let appversion =
  Printf.sprintf
    "%0d.%0d.%0d.%s"
    version_major
    version_minor
    version_patch
    version_pre_release
;;

let apppath = Sys.argv.(0) |> Unix.realpath
let approot = Filename.dirname (Filename.dirname apppath)
let appcwd = Sys.getcwd ()
let appmode = Sys.getenv_opt "OXBANG_MODE" |> Option.value ~default:"development"
let conf confname = Printf.sprintf "./config/%s/%s.json" appmode confname

module type Config = sig
  type t [@@deriving yojson]

  val configs : string
end

module Make (C : Config) = struct
  type t = C.t [@@deriving yojson]

  let configs = C.configs
end

let json_string_load json =
  if Sys.file_exists json then Json.from_file json else Json.from_string "{}"
;;

let json_load json_file = Json.from_file json_file

let json_get_string key json =
  let open Json.Util in
  member key json |> to_string_option
;;

let conf_for (type a) m =
  let module C = Make ((val m : Config with type t = a)) in
  let json =
    json_string_load C.configs
    (*if Sys.file_exists C.configs then Json.from_file C.configs
      else Json.from_string "{}"*)
  in
  C.t_of_yojson json
;;

module ServerConf = struct
  type t =
    { interface : string [@default "localhost"]
    ; port : int [@default 8080]
    ; domains : int [@default 2]
    ; connections : int [@default 128]
    ; log_level : string [@default "info"]
    ; tls : bool [@default false]
    ; certificate_file : string [@default ""]
    ; key_file : string [@default ""]
    ; acao: string list [@default []]
    ; acam: string list [@default []]

    ; public : string [@default "./public"]
    ; static : string [@default "./public-static"]
    ; session : string [@default "cookie"]
    }
  [@@deriving yojson]

  let configs = conf "server"
end

module HashingConf = struct
  type t =
    { engine : string [@default "Argon2i"]
    ; iterations : int [@default 2]
    ; memory : int [@default 65536]
    ; parallelism : int [@default 1]
    ; hash_len : int [@default 32]
    ; salt : string [@default "0000000000000000"]
    ; password_size : int [@default 12]
    }
  [@@deriving yojson]

  let configs = conf "hashing"
end

let serverConf = conf_for (module ServerConf)

let absPublicPath =
  Filename.concat appcwd serverConf.public |> Unix.realpath
;;

let hashingConf = conf_for (module HashingConf)
