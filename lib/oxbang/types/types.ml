(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

module Vars = struct
  type t =
    | VStrL of string list
    | VIntL of int list
    | VStr of string
    | VInt of int
end

module Verbs = struct
  type callback_t =
    (string * Vars.t) list -> Http.Request.t -> Eio.Flow.source_ty Eio.Resource.t
end

let aaa
      ?(env : Eio_unix.Stdenv.base option = None)
      ?(params : (string * Vars.t) list option = None)
      (request : Http.Request.t)
  =
  ignore env;
  ignore params;
  ignore request;
  Cohttp_eio.Body.of_string ""
;;
