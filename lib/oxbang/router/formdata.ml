(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

(* Part of: https://github.com/dinosaure/multipart_form *)
module Map = Map.Make (String)

let to_map ~assoc m =
  let open Multipart_form in
  let rec go (map, rest) = function
    | Leaf { header; body } ->
      let filename =
        Option.bind (Header.content_disposition header) Content_disposition.filename
      in
      (match Option.bind (Header.content_disposition header) Content_disposition.name with
       | Some name -> Map.add name (filename, List.assoc body assoc) map, rest
       | None -> map, (body, (filename, List.assoc body assoc)) :: rest)
    | Multipart { body; _ } ->
      let fold acc = function
        | Some elt -> go acc elt
        | None -> acc
      in
      List.fold_left fold (map, rest) body
  in
  go (Map.empty, []) m
;;

let is_multipart ct = Regexp.contains {|multipart\/form-data|} ct

let body_to_params request body =
  let body_source = Eio.Flow.read_all body in
  let content_type = Helpers.content_type_as_str request in
  match content_type with
  | "application/x-www-form-urlencoded" -> Uri.query_of_encoded body_source
  | ct when is_multipart ct ->
    (match Multipart_form.Content_type.of_string (content_type ^ "\r\n") with
     | Ok ctype ->
       (match Multipart_form.of_string_to_list body_source ctype with
        | Ok (m, assoc) ->
          let m, _r = to_map ~assoc m in
          let m = Map.bindings m in
          List.fold_left
            (fun memo m ->
              let k, (_, v) = m in
              (k, [ v ]) :: memo)
            []
            m
        | _ -> [])
     | _ -> [])
  | _ -> []
;;
