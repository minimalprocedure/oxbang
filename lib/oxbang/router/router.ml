(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

open Helpers

let response_add_headers headers_list response =
  let headers = Http.Response.headers response
  and version = Http.Response.version response
  and status = Http.Response.status response in
  let headers = Http.Header.add_list headers headers_list in
  Http.Response.make ~headers ~version ~status ()
;;

let response_cors_headers request =
  match Http.Header.get (Http.Request.headers request) "Origin" with
  | None -> []
  | Some origin ->
    let acao = Confs.serverConf.acao in
    (match acao with
     | [] -> []
     | wildcard :: _ when String.trim wildcard = "*" ->
       [ "Access-Control-Allow-Origin", "*" ]
     | origins ->
       if List.exists (fun o -> o = origin) origins
       then [ "Access-Control-Allow-Origin", origin; "Vary", "Origin" ]
       else [])
;;

let default_headers request =
  response_cors_headers request
  @ [ "Server", Printf.sprintf "%s/%s" Confs.appname Confs.appversion
    ; "Date", Ptime.to_rfc3339 (Ptime_clock.now ())
    ]
;;

let resolve_route routes request body =
  Array.find_mapi
    (fun index route ->
       let tpl_parts, _ = route in
       match Analyzer.analyze_and_params tpl_parts request body with
       | None -> None
       | Some params -> Some (index, params))
    routes
;;

let process_static_request local request =
  let headers = Http.Header.init () in
  match Http.Header.get (Http.Request.headers request) "Range" with
  | Some req_ranges ->
    let data = Helpers.File_h.read_partial_file ~req_ranges local in
    (match data with
     | Ok (data, low, high, fsize) ->
       let headers =
         Http.Header.add_list
           headers
           [ "Accept-Ranges", "bytes"
           ; "Content-Length", string_of_int (String.length data)
           ; "Content-Range", Printf.sprintf "bytes %d-%d/%d" low high fsize
           ; "Keep-Alive", "timeout=30"
           ]
       in
       Logger.debug (fun m ->
         m
           "Data ranges: fileSize: %d dataLen: %d low: %d high: %d"
           fsize
           (String.length data)
           low
           high);
       headers, data, Http.Status.of_int 206
     | Error _ ->
       (* 416 Range Not Satisfiable *)
       headers, "", Http.Status.of_int 416)
  | None -> headers, Eio.Path.load local, Http.Status.of_int 200
;;

let resolve_static ?(name = "index") ?(ext = ".html") ?(response_headers = []) request =
  let ( / ) = Eio.Path.( / ) in
  let target = Http.Request.resource request in
  let relative_local =
    Filename.concat Confs.serverConf.public target
    |> File_h.dedup_pathsep
    |> Uri.pct_decode
  in
  let env = Global.env () in
  let local = Eio.Stdenv.cwd env / relative_local in
  let local = if Eio.Path.is_directory local then local / (name ^ ext) else local in
  if Eio.Path.is_file local
  then (
    let fdate = Eio.Path.stat ~follow:true local in
    let mtime =
      Option.value
        ~default:(Ptime_clock.now ())
        (Eio.File.Stat.(fdate.mtime) |> Ptime.of_float_s)
      |> Ptime.to_rfc3339
    in
    let headers, body, status = process_static_request local request in
    let _, local_s = local in
    let headers =
      Http.Header.add_list
        headers
        ([ "Content-Type", Magic_mime.lookup local_s; "Last-Modified", mtime ]
         @ response_headers)
    in
    ( Cohttp_eio.Server.respond ~headers ~status ~body:(Cohttp_eio.Body.of_string body) ()
    , status ))
  else (
    let body = "<strong>Not Found</strong>" in
    let status = `Not_found in
    ( Cohttp_eio.Server.respond
        ~headers:(Http.Header.of_list [ "content-type", "text/html" ])
        ~status
        ~body:(Cohttp_eio.Body.of_string body)
        ()
    , status ))
;;

let route
      ~routes
      (_conn : Cohttp_eio.Server.conn)
      (request : Http.Request.t)
      (body : Cohttp_eio.Body.t)
  =
  let response, status =
    match resolve_route routes request body with
    | Some (index, params) ->
      let _, (headers, status, callback) = routes.(index) in
      let headers = Http.Header.add_list headers (default_headers request) in
      ( Cohttp_eio.Server.respond ~headers ~status ~body:(callback params request) ()
      , status )
    | None -> resolve_static ~response_headers:(default_headers request) request
  in
  Rlog.log_request request status;
  response
;;
