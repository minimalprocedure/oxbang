(**
 * Copyright (c) 2025 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let log_request_response request response =
  let meth = Http.Request.meth request |> Http.Method.to_string in
  let target = Http.Request.resource request in
  let status = Http.Response.status response |> Http.Status.to_int in
  Logger.info (fun m -> m "%d %s: %s" status meth target)
;;

let log_request request status =
  let meth = Http.Request.meth request |> Http.Method.to_string in
  let target = Http.Request.resource request in
  let status = Http.Status.to_int status in
  Logger.info (fun m -> m "%d %s: %s" status meth target)
;;
