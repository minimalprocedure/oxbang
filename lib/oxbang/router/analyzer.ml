(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

module Types = Types
module Vars = Vars
module Formdata = Formdata

let compare_parts tpl_parts req_parts =
  let lzip xs ys = List.map2 (fun a b -> a, b) xs ys in
  let parts = lzip tpl_parts req_parts in
  List.filter
    (fun e ->
       let tp, up = e in
       Vars.is_var tp || up = tp)
    parts
;;

let get_meth_uri request =
  let meth = Http.Request.meth request |> Cohttp.Code.string_of_method in
  let uri = Http.Request.resource request |> Uri.of_string in
  String.uppercase_ascii meth, uri
;;

let splat tpl_parts req_parts =
  if Helpers.ll_short tpl_parts req_parts
  then (
    match List.find_index (fun e -> e = "**") tpl_parts with
    | None -> None
    | Some i -> Some (List.drop i req_parts |> String.concat "/"))
  else None
;;

let analyze_and_params tpl_parts request body =
  let meth, uri = get_meth_uri request in
  let req_parts = meth :: String.split_on_char '/' (Uri.path uri) in
  match splat tpl_parts req_parts with
  | None ->
    if Helpers.ll_equ tpl_parts req_parts
    then (
      let scanned = compare_parts tpl_parts req_parts in
      if Helpers.ll_equ scanned tpl_parts
      then (
        let vars =
          List.fold_left
            (fun vars e ->
               let k, v = e in
               if Vars.is_var k then (k, Regexp.split_on_comma v) :: vars else vars)
            []
            scanned
        in
        let formdata = Formdata.body_to_params request body in
        let params =
          List.map (fun v -> Vars.process_var v) (formdata @ vars @ Uri.query uri)
        in
        Some params)
      else None)
    else None
  | Some path ->
    let vars = [ "__splat__", [ path ] ] in
    let formdata = Formdata.body_to_params request body in
    let params =
      List.map (fun v -> Vars.process_var v) (formdata @ vars @ Uri.query uri)
    in
    Some params
;;
