(**
 * Copyright (c) 2025 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let environ = ref None
let init e = environ := Some e

let env () =
  match !environ with
  | None -> failwith "...unavailable env!"
  | Some e -> e
;;
