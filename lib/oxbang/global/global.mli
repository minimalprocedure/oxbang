(**
 * Copyright (c) 2025 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

val init : Eio_unix.Stdenv.base -> unit
val env : unit -> Eio_unix.Stdenv.base
