(** * Copyright (c) 2022 - 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

let hash_kind = function
  | "Argon2i" -> Argon2.I
  | "Argon2d" -> Argon2.D
  | "Argon2id" -> Argon2.ID
  | _ -> Argon2.I
;;

let hash_kind_m (confs : Confs.HashingConf.t) =
  let kind = Confs.HashingConf.(confs.engine) in
  match kind with
  | "Argon2i" ->
    (module struct
      include Argon2.I
    end : Argon2.HashFunctions)
  | "Argon2d" ->
    (module struct
      include Argon2.D
    end : Argon2.HashFunctions)
  | "Argon2id" ->
    (module struct
      include Argon2.ID
    end : Argon2.HashFunctions)
  | _ ->
    (module struct
      include Argon2.I
    end : Argon2.HashFunctions)
;;

let hash_password ?(exsalt = "") pwd confs =
  let open Confs.HashingConf in
  let t_cost = confs.iterations in
  let m_cost = confs.memory in
  let parallelism = confs.parallelism in
  let salt = confs.salt ^ exsalt in
  let salt_len = String.length salt in
  let kind = hash_kind confs.engine in
  let hash_len = confs.hash_len in
  let version = Argon2.VERSION_NUMBER in
  let module Argon2Kind = (val hash_kind_m confs) in
  let encoded_len =
    Argon2.encoded_len ~t_cost ~m_cost ~parallelism ~salt_len ~hash_len ~kind
  in
  let hash =
    Argon2.hash
      ~t_cost
      ~m_cost
      ~parallelism
      ~pwd
      ~salt
      ~kind
      ~hash_len
      ~encoded_len
      ~version
  in
  match hash with
  | Result.Ok result ->
    let _hash, encoded = result in
    encoded
  | Result.Error _e ->
    print_endline "Error while computing hash";
    ""
;;
