(** * Copyright (c) 2022 - 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

let string_of_char c = String.make 1 c
let lcase = "abcdefghijklmnopqrstuvwxyz"
let ucase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
let digit = "0123456789"
let symbols = "!#$%&'()*+,-./:;<=>?@[]^_{|}~"
let chars = Printf.sprintf "%s%s%s%s" lcase ucase digit symbols

let rnd_order () =
  match Random.int 3 with
  | 0 -> -1
  | 1 -> 0
  | 2 -> 1
  | _ -> 0
;;

let shuffle () =
  let chars = Printf.sprintf "%s%s%s%s" lcase ucase digit symbols in
  let size = String.length chars - 1 in
  let chars = List.init size (fun i -> string_of_char chars.[i]) in
  List.sort (fun _ _ -> rnd_order ()) chars |> String.concat ""
;;

let rand_pass ?(len = Confs.hashingConf.password_size) () =
  let len =
    if len < Confs.hashingConf.password_size then Confs.hashingConf.password_size else len
  in
  let shuffled = shuffle () in
  let size = String.length shuffled - 1 in
  List.init len (fun _ -> string_of_char shuffled.[Random.int size]) |> String.concat ""
;;

let is_right_length password = String.length password >= Confs.hashingConf.password_size
