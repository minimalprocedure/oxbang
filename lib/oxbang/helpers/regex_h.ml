(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

type regex_engine =
  | PerlEngine
  | PcreEngine

let regex ?(engine = PcreEngine) regex_str =
  match engine with
  | PerlEngine -> Re.Perl.re ~opts:[ `Dotall; `Caseless ]regex_str |> Re.Perl.compile
  | PcreEngine -> Re.Pcre.regexp ~flags:[ `CASELESS ] regex_str
;;

let scan_source rex_attrs source = Re.all (regex rex_attrs) source


