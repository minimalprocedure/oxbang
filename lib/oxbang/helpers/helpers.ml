(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

let absolutize uri = if String.starts_with ~prefix:"/" uri then uri else "/" ^ uri
let req_headers_to_list request = Http.Request.headers request |> Http.Header.to_list
let res_headers_to_list response = Http.Response.headers response |> Http.Header.to_list

let content_type request =
  let headers = Http.Request.headers request in
  Http.Header.get headers "content-type"
;;

let content_type_as_str request =
  match content_type request with
  | Some ct -> ct
  | _ -> ""
;;

module File_h = struct
  let dir_sep = Filename.dir_sep
  let dir_sep_char = String.get dir_sep 0

  let path_target_to_os path =
    let url_sep = '/' in
    let dir_sep = Filename.dir_sep in
    let combine list = list |> List.filter (fun s -> s <> "") |> String.concat dir_sep in
    match String.split_on_char url_sep path with
    | h :: t when h = "" -> Printf.sprintf "%s%s" dir_sep (combine t)
    | parts -> combine parts
  ;;

  let dedup_pathsep ?(sep = '/') path =
    let ssep = String.make 1 sep in
    let combine list = list |> List.filter (fun s -> s <> "") |> String.concat ssep in
    match String.split_on_char sep path with
    | h :: t when h = "" -> Printf.sprintf "%c%s" sep (combine t)
    | parts -> combine parts
  ;;

  let open_file filename =
    let ic = In_channel.open_text filename in
    let source = In_channel.input_all ic in
    In_channel.close ic;
    source
  ;;

  let decode_request_ranges max ranges =
    let matches = Regex_h.scan_source {|(\d+)?-(\d+)?,?\s*|} ranges in
    List.fold_left
      (fun acc mtch ->
         let m = Re.Group.get_opt mtch in
         let low = Option.fold ~none:None ~some:(fun n -> Some (int_of_string n)) (m 1) in
         let high =
           Option.fold ~none:None ~some:(fun n -> Some (int_of_string n)) (m 2)
         in
         match low, high with
         | None, None -> acc
         | Some low, Some high when high <= low -> acc
         | Some low, Some high when high > max -> (low, max) :: acc
         | Some low, Some high -> (low, high) :: acc
         | None, Some high -> (max - high, max) :: acc
         | Some low, None -> (low, max) :: acc)
      []
      matches
    |> List.rev
  ;;

  let ( / ) = Eio.Path.( / )

  let read_file_range ~fsize ~low ~high infile =
    let nbytes = high - low in
    let buff = Eio.Buf_read.of_flow ~initial_size:high ~max_size:fsize infile in
    let _ =
      if low <= 0
      then Eio.Buf_read.ensure buff high
      else (
        Eio.Buf_read.ensure buff low;
        Eio.Buf_read.consume buff low;
        Eio.Buf_read.ensure buff nbytes)
    in
    let data = Cstruct.to_string (Eio.Buf_read.peek buff) in
    data
  ;;

  let read_partial_file ~req_ranges fname =
    Eio.Path.with_open_in fname
    @@ fun infile ->
    let fsize = Eio.File.size infile |> Optint.Int63.to_int in
    let ranges = decode_request_ranges fsize req_ranges in
    match ranges with
    | range :: _ ->
      let low, high = range in
      let partial = read_file_range ~fsize ~low ~high:(high + 1) infile in
      Result.ok (partial, low, high, fsize)
    | _ -> Result.error "empty ranges/more then one"
  ;;
end

let ll_equ a b = List.compare_lengths a b = 0
let ll_short a b = List.compare_lengths a b = -1
let ll_tall a b = List.compare_lengths a b = 1

let trim_indent s =
  let rex =  Re.Pcre.regexp ~flags:[ `CASELESS; `MULTILINE ] {|^\s*|} in
  Re.replace_string ~all:true rex s ~by:""
;;
