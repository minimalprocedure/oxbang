(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

let start_message ~port ~domains ~nconn =
  let msg = Printf.sprintf
    {|
    %s ver.: %s start on port: %d with %d domains and %d connections.
    cwd: %s
    public: %s
    |}
    Confs.appname
    Confs.appversion
    port
    domains
    nconn
    Confs.appcwd
    Confs.absPublicPath
  |> Helpers.trim_indent in 
  print_endline msg
;;

let start ~routes ?(port = 3000) ?(domains = 0) ?(nconn = 128) () =
  start_message ~port ~domains ~nconn;
  let routes = Array.of_list routes in
  Eio_main.run
  @@ fun env ->
  Global.init env;
  Eio.Switch.run
  @@ fun sw ->
  let max_domains = Domain.recommended_domain_count () in
  let domains =
    match domains with
    | doms when doms < 0 -> 0
    | doms when doms > max_domains -> max_domains
    | _ -> domains
  in
  let addr = `Tcp (Eio.Net.Ipaddr.V4.loopback, port) in
  let backlog = nconn * (domains - 1) in
  let socket = Eio.Net.listen env#net ~sw ~backlog ~reuse_addr:true addr in
  (*let server = Cohttp_eio.Server.make ~callback:(respond ~env ~routes) () in*)
  let server = Cohttp_eio.Server.make ~callback:(Router.route ~routes) () in
  Cohttp_eio.Server.run
    socket
    server
    ~additional_domains:(Eio.Stdenv.domain_mgr env, domains)
    ~on_error:Logger.exn
;;

let start_with_confs ~routes =
  start
    ~routes
    ~port:Confs.serverConf.port
    ~domains:Confs.serverConf.domains
    ~nconn:Confs.serverConf.connections
;;
