(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

module Reporter = Reporter [@@warning "-27"]

let src = Logs.Src.create "OxBang.server" ~doc:"OxBang server logger"

module Log = (val Logs.src_log src : Logs.LOG)

let conf_level =
  match String.uppercase_ascii Confs.serverConf.log_level with
  | "APP" -> Logs.App
  | "ERROR" -> Logs.Error
  | "WARNING" -> Logs.Warning
  | "INFO" -> Logs.Info
  | "DEBUG" -> Logs.Debug
  | _ -> Logs.Info
;;

(*let () = Logs.set_reporter (Logs_fmt.reporter ())*)
let () = Fmt_tty.setup_std_outputs ()
and () = Logs.set_reporter (Reporter.reporter ())
and () = Logs_threaded.enable ()
and () = Logs.Src.set_level Cohttp_eio.src (Some Info)
and () = Logs.Src.set_level src (Some conf_level)

let exn ex = Logs.warn (fun f -> f "%a" Eio.Exn.pp ex)
let info f = Logs.info ~src f
let debug f = Logs.debug ~src f
let error f = Logs.err ~src f
let warn f = Logs.warn ~src f
