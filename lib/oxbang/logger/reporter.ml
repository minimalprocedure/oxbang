(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

(*let stamp_tag : Mtime.span Logs.Tag.def =
  Logs.Tag.def "stamp" ~doc:"Relative monotonic time stamp" Mtime.Span.pp
  ;;

  let stamp c = Logs.Tag.(empty |> add stamp_tag (Mtime_clock.count c))*)

type log_level =
  [ `Error
  | `Warning
  | `Info
  | `Debug
  ]

let to_logs_level l =
  match l with
  | `Error -> Logs.Error
  | `Warning -> Logs.Warning
  | `Info -> Logs.Info
  | `Debug -> Logs.Debug
;;

let level_and_style level =
  match level with
  | Logs.App -> `White, "     "
  | Logs.Error -> `Red, "ERROR"
  | Logs.Warning -> `Yellow, " WARN"
  | Logs.Info -> `Green, " INFO"
  | Logs.Debug -> `Blue, "DEBUG"
;;

let now () = Ptime.to_float_s (Ptime.v (Pclock.now_d_ps ()))

let format_now_time () =
  let unix_time = now () in
  let time = Option.get (Ptime.of_float_s unix_time) in
  let fraction = fst (modf unix_time) *. 1000. in
  let clamped_fraction = if fraction > 999. then 999. else fraction in
  let (y, m, d), ((hh, mm, ss), _tz_offset_s) = Ptime.to_date_time time in
  Printf.sprintf
    "%02i.%02i.%02i %02i:%02i:%02i.%03.0f"
    d
    m
    (y mod 100)
    hh
    mm
    ss
    clamped_fraction
;;

let format_source src =
  let width = 15 in
  if Logs.Src.name src = Logs.Src.name Logs.default
  then String.make width ' '
  else (
    let name = Logs.Src.name src in
    if String.length name > width
    then String.sub name (String.length name - width) width
    else String.make (width - String.length name) ' ' ^ name)
;;

let flush_buffer_and_print buffer =
  let message = Buffer.contents buffer in
  Buffer.reset buffer;
  prerr_string message;
  Stdlib.flush stderr
;;

let reporter () =
  (* Format into an internal buffer. *)
  let buffer = Buffer.create 512 in
  let ppf = Fmt.with_buffer ~like:Fmt.stderr buffer in
  let report src level ~over k user_msgf =
    let level_style, level = level_and_style level in
    let write _ =
      flush_buffer_and_print buffer;
      over ();
      k ()
    in
    let stamp_log ?header ?tags k ppf fmt =
      ignore header;
      ignore tags;
      let time = format_now_time () in
      let source = format_source src in
      let source_prefix, source =
        try
          let dot_index = String.rindex source '.' + 1 in
          ( String.sub source 0 dot_index
          , String.sub source dot_index (String.length source - dot_index) )
        with
        | Not_found -> "", source
      in
      let request_id = None in
      let request_id, request_style =
        match request_id with
        | Some "" | None -> "", `White
        | Some request_id ->
          (* The last byte of the request id is basically always going to be a
             digit, growing incrementally, so we can use the parity of its
             ASCII code to stripe the requests in the log. *)
          let last_byte = request_id.[String.length request_id - 1] in
          let color = if Char.code last_byte land 1 = 0 then `Cyan else `Magenta in
          " --- " ^ request_id, color
      in
      Format.kfprintf
        k
        ppf
        ("%a %a%s %a%a @[" ^^ fmt ^^ "@]@.")
        Fmt.(styled `Faint string)
        time
        Fmt.(styled `White string)
        source_prefix
        source
        Fmt.(styled level_style string)
        level
        Fmt.(styled request_style (styled `Italic string))
        request_id
    in
    user_msgf @@ fun ?header ?tags fmt -> stamp_log ~header ~tags write ppf fmt
  in
  { Logs.report }
;;

