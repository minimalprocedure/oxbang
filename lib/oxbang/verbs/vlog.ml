(**
 * Copyright (c) 2025 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let debug_params params =
  if Logger.conf_level = Logs.Debug
  then
    List.iter
      (fun (k, v) -> Logger.debug (fun m -> m "[PAR]: %s:%s" k (Vars.str_of_var v)))
      params
  else ()
;;

let debug_headers_list headers =
  if Logger.conf_level = Logs.Debug
  then List.iter (fun (k, v) -> Logger.debug (fun m -> m "[REQ]: %s:%s" k v)) headers
  else ()
;;

let debug_req_headers request = debug_headers_list (Helpers.req_headers_to_list request)
let debug_res_headers response = debug_headers_list (Helpers.res_headers_to_list response)

let debug_pass uri pass =
  if Logger.conf_level = Logs.Debug
  then Logger.debug (fun m -> m "[URI]: %s -> %s" uri pass)
  else ()
;;
