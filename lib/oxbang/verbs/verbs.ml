(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

module Output = struct
  let out_string = Cohttp_eio.Body.of_string
end

module FailResponse = struct
  let not_found () =
    Cohttp_eio.Server.respond
      ~headers:(Http.Header.of_list [ "content-type", "text/html" ])
      ~status:`Not_found
  ;;
end

let extract_params key params =
  let data = List.find_opt (fun (k, _) -> k = key) params in
  Option.fold ~none:None ~some:(fun (_, v) -> Some (Vars.str_of_var v)) data
;;

let verb
      ~(meth : Http.Method.t (*Cohttp.Code.meth*))
      ~uri
      ?(headers = [ "Content-Type", "text/html" ])
      ?(status : Http.Status.t = `OK)
      (callback : Types.Verbs.callback_t)
  =
  let headers = Http.Header.of_list headers in
  let tpl_parts =
    Http.Method.to_string meth :: (Helpers.absolutize uri |> String.split_on_char '/')
  in
  tpl_parts, (headers, status, callback)
;;

let get ~uri callback = verb ~meth:`GET ~uri callback
let post ~uri callback = verb ~meth:`POST ~uri callback

let normalize_headers request =
  let headers = Cohttp.Request.headers request in
  let req_uri = Cohttp.Request.uri request in
  let scheme =
    match Uri.scheme req_uri with
    | None -> "http"
    | Some sch -> sch
  in
  let remove key headers = Http.Header.remove headers key in
  let replace key value headers = Http.Header.replace headers key value in
  let host = Http.Header.get headers "Host" |> Option.get in
  remove "Host" headers
  |> remove "Connection"
  |> remove "Keep-Alive"
  |> remove "Proxy-Authenticate"
  |> remove "Proxy-Authorization"
  |> remove "TE"
  |> remove "Trailers"
  |> remove "Transfer-Encoding"
  |> remove "Upgrade"
  |> replace "Accept-Encoding" "identity"
  |> replace "Cache-Control" "no-transform"
  |> replace "X-Forwarded-Host" host
  |> replace "X-Forwarded-Proto" scheme
  |> replace "X-Forwarded-For" "hidden"
  |> replace
       "Forwarded"
       (Printf.sprintf "by=hidden;for=hidden;host=%s;proto=%s;" host scheme)
;;

let get_pass ~uri ~pass =
  verb ~meth:`GET ~uri (fun params request ->
    let splat = extract_params "__splat__" params in
    let pass =
      match splat with
      | None -> pass
      | Some s -> Printf.sprintf "%s/%s" pass s
    in
    (********************************
    Vlog.debug_req_headers request;
    Vlog.debug_params params;
    Vlog.debug_pass uri pass;
    ********************************)
    let pass_uri = Uri.of_string pass in
    let headers = normalize_headers request in
    let sresponse = Vproxy.apply ~meth:`GET ~uri:pass_uri ~headers () in
    Output.out_string sresponse)
;;
