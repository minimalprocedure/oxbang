(**
 * Copyright (c) 2025 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

open Cohttp_eio

let () = Mirage_crypto_rng_unix.use_default ()

let authenticator =
  match Ca_certs.authenticator () with
  | Ok x -> x
  | Error (`Msg m) ->
    Fmt.failwith "Failed to create system store X509 authenticator: %s" m
;;

let https ~authenticator =
  let tls_config =
    match Tls.Config.client ~authenticator () with
    | Error (`Msg msg) -> failwith ("tls configuration problem: " ^ msg)
    | Ok tls_config -> tls_config
  in
  fun uri raw ->
    let host =
      Uri.host uri |> Option.map (fun x -> Domain_name.(host_exn (of_string_exn x)))
    in
    Tls_eio.client_of_flow ?host tls_config raw
;;

let client_make ?(https = None) () =
  let env = Global.env () in
  Client.make ~https env#net
;;

let apply ~(meth : Http.Method.t (*Cohttp.Code.meth*)) ~uri ?headers ?req_body () =
  (*let headers = Http.Header.of_list headers in*)
  let client = client_make ~https:(Some (https ~authenticator)) () in
  Eio.Switch.run
  @@ fun sw ->
  let apply =
    match meth with
    | `GET -> Client.get ~sw client ?headers
    | `POST ->
      let req_body = Cohttp_eio.Body.of_string (Option.get req_body) in
      Client.post ~sw client ?headers ~chunked:true ~body:req_body
    | _ -> failwith "Method not allowed"
  in
  let resp, body = apply uri in
  let response =
    match resp.status with
    | `OK -> Eio.Buf_read.(parse_exn take_all) body ~max_size:max_int
    | _ ->
      let msg = Fmt.str "Unexpected HTTP status: %a" Http.Status.pp resp.status in
      Printf.sprintf "%s" msg
  in
  response
;;
