(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

let html_vars params request =
  List.iter
    (fun p ->
       let k, v = p in
       Eio.traceln "params: k -> %s, v -> %s" k (Vars.str_of_var v))
    params;
  Verbs.Output.out_string (Http.Request.resource request)
;;

let html_post params request =
  List.iter
    (fun p ->
       let k, v = p in
       Eio.traceln "params: k -> %s, v -> %s" k (Vars.str_of_var v))
    params;
  Verbs.Output.out_string (Http.Request.resource request)
;;

let sono_pippo _params _request = Verbs.Output.out_string "sono pippo"
let sono_root _params _request = Verbs.Output.out_string "sono root"

let publics =
  [ Verbs.post ~uri:"/post" html_post
  ; Verbs.get ~uri:"/html" sono_pippo
  ; Verbs.get ~uri:"/html/:variable" html_vars
  ; Verbs.get ~uri:"/" sono_root  
  ; Verbs.get_pass ~uri:"pragmas" ~pass:"https://minimalprocedure.pragmas.org"
  ; Verbs.get_pass ~uri:"proxy_https" ~pass:"https://example.com"
  ; Verbs.get_pass ~uri:"proxy/**" ~pass:"https://minimalprocedure.pragmas.org"
  ]
[@@warning "-27"]
;;
